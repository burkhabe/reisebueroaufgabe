package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class AbstractAsienreiseFactory implements AbstractPauschalreiseFactory{

    @Override
    public ReisezielDecorator generiere7Tage(Reiseklasse reiseklasse) {
        ReisezielDecorator siebenTageReise = new FluganreiseLangstrecke();

        //Tage hinzufügen
        siebenTageReise = new Bangkok1Tag(new Bangkok1Tag(new Bangkok1Tag(new KohChang1Tag(new KohChang1Tag(new KohChang1Tag(siebenTageReise))))));

        //Reisebuero.Reiseklasse anpassen
        siebenTageReise.setReiseklasse(reiseklasse);

        //Versicherung hinzufügen
        siebenTageReise = new ReiseruecktrittsVersicherung(siebenTageReise);

        return siebenTageReise;
    }

    @Override
    public ReisezielDecorator generiere10Tage(Reiseklasse reiseklasse) {

        return new KohKood1Tag(new KohKood1Tag(new KohKood1Tag(generiere7Tage(reiseklasse))));
    }

    @Override
    public ReisezielDecorator generiere16Tage(Reiseklasse reiseklasse) {

        ReisezielDecorator sechzehnTageReise = new FluganreiseLangstrecke();

        for(int i=0; i<16;i++){
            sechzehnTageReise = new Bangkok1Tag(sechzehnTageReise);
        }

        //Reisebuero.Reiseklasse anpassen
        sechzehnTageReise.setReiseklasse(reiseklasse);

        //Versicherung hinzufügen
        sechzehnTageReise = new ReiseruecktrittsVersicherung(sechzehnTageReise);

        return sechzehnTageReise;
    }



    @Override
    public void druckeBeschreibung(ReisezielDecorator reisezielDecorator){
        System.out.println(reisezielDecorator.getBeschreibung());
        System.out.println("---------------------------\nGesamtpreis:\t" +reisezielDecorator.getPreis());
    }
}