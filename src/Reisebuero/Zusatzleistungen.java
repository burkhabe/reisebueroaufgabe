package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public abstract class Zusatzleistungen implements ReisezielDecorator {
    //protected, damit auch die erbenden Klassen darauf zugreifen können
    protected final ReisezielDecorator reisezielDecorator;

    public Zusatzleistungen(ReisezielDecorator reisezielDecorator){
        this.reisezielDecorator = reisezielDecorator;
    }
}
