package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class KohChang1Tag extends Orte {

    private final Money preis = new Money(4000);
    private final double aufpreisFaktor = 0.7;

    @Override
    public Reiseklasse getReiseklasse() {
        return reisezielDecorator.getReiseklasse();
    }

    @Override
    public void setReiseklasse(Reiseklasse reiseklasse) {
        reisezielDecorator.setReiseklasse(reiseklasse);
    }


    public KohChang1Tag(ReisezielDecorator reisezielDecorator) {
        super(reisezielDecorator);
    }

    @Override
    public Money getPreis() {
        return reisezielDecorator.getPreis().add(preis.add(reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor)));
    }

    @Override
    public String getBeschreibung() {
        return reisezielDecorator.getBeschreibung() + reisezielDecorator.getReiseklasse().sterne() + "1 Tag\tKoh Chang:\tDie Elefanteninsel. Koh Chang besticht mit Naturbelassenheit und tollen Stränden.\n\t\t["+preis+ " + " + reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor) + "]\n\n";
    }

}
