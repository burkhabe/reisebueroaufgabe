package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class ReiseruecktrittsVersicherung extends Zusatzleistungen {


    private final Money preis = new Money(10000);

    public ReiseruecktrittsVersicherung(ReisezielDecorator reisezielDecorator){
        super(reisezielDecorator);

    }

    @Override
    public Money getPreis() {
        //+ 100€
        return reisezielDecorator.getPreis().add(preis);
    }

    @Override
    public String getBeschreibung() {
        return reisezielDecorator.getBeschreibung() + reisezielDecorator.getReiseklasse().sterne() + "Reiserücktrittsversicherung gebucht!\n\t\t["+preis+"]\n\n";
    }

    @Override
    public Reiseklasse getReiseklasse() {
        return reisezielDecorator.getReiseklasse();
    }

    @Override
    public void setReiseklasse(Reiseklasse reiseklasse) {
        reisezielDecorator.setReiseklasse(reiseklasse);
    }
}
