package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class Reisebuero {
    public static void main(String[] args) {


        //DECORATOR PATTERN - Dekoriert ein en Flug mit Hotels und Reisebuero.Zusatzleistungen
        ReisezielDecorator meineFlugreise = new Bangkok1Tag(new FluganreiseLangstrecke());

        meineFlugreise = new KohKood1Tag(meineFlugreise);

        //STRATEGIE PATTERN - setzt für die komplette Reise eine Reisebuero.Reiseklasse
        meineFlugreise.setReiseklasse(new FuenfSterne());

        //Hinzufügen der Reisenrücktrittsversicherung
        meineFlugreise = new ReiseruecktrittsVersicherung(meineFlugreise);


        //Kategorie kann jederzeit verändert werden [Wechsel der Strategie]
        meineFlugreise.setReiseklasse(new DreiSterne());


        System.out.println(meineFlugreise.getBeschreibung());
        System.out.println("---------------------------\nGesamtpreis:\t" +meineFlugreise.getPreis());

        System.out.println("--------------------------------------------------------------------------------------");


        //ABSTRACT FACTORY - Mit der Factory werden Pauschalreisen generiert die trotzdem anschließend noch ergänzt werden können sollen

        AbstractAsienreiseFactory asienreiseFactory = new AbstractAsienreiseFactory();
        ReisezielDecorator asienReise7 = asienreiseFactory.generiere7Tage(new FuenfSterne());
        asienreiseFactory.druckeBeschreibung(asienReise7);

        ReisezielDecorator asienReise10 = asienreiseFactory.generiere10Tage(new VierSterne());
        asienreiseFactory.druckeBeschreibung(asienReise10);


        ReisezielDecorator asienReise16 = asienreiseFactory.generiere16Tage(new DreiSterne());
        asienreiseFactory.druckeBeschreibung(asienReise16);


    }
}
