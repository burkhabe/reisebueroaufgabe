package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class KohKood1Tag extends Orte {

    private final Money preis = new Money(3500);
    private final double aufpreisFaktor = 0.5;


    @Override
    public Reiseklasse getReiseklasse() {
        return reisezielDecorator.getReiseklasse();
    }

    @Override
    public void setReiseklasse(Reiseklasse reiseklasse) {
        reisezielDecorator.setReiseklasse(reiseklasse);
    }

    public KohKood1Tag(ReisezielDecorator reisezielDecorator) {
        super(reisezielDecorator);
    }

    @Override
    public Money getPreis() {
        return reisezielDecorator.getPreis().add(preis.add(reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor)));
    }

    @Override
    public String getBeschreibung() {
        return reisezielDecorator.getBeschreibung() + reisezielDecorator.getReiseklasse().sterne() + "1 Tag\tKoh Kood:\tEine wundervoll Insel die noch kaum vom Tourismus geprägt ist.\n\t\t["+preis+ " + " + reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor) + "]\n\n";
    }
}
