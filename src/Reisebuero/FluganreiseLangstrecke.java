package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class FluganreiseLangstrecke implements ReisezielDecorator {

    private final Money preis = new Money(50000);
    private final double aufpreisFaktor = 30.5;

    //nicht final, um späteren Strategiewechsel zu ermöglichen
    private Reiseklasse reiseklasse = new StandardZweiSterne();

    @Override
    public void setReiseklasse(Reiseklasse reiseklasse) {
        this.reiseklasse = reiseklasse;
    }

    @Override
    public Reiseklasse getReiseklasse() {
        return this.reiseklasse;
    }

    @Override
    public Money getPreis() {

        return preis.add(reiseklasse.aufpreis().multiply(aufpreisFaktor));
    }

    @Override
    public String getBeschreibung() {
        return "=== Langstreckenflugreise mit folgenden Komponenten: ===\n\n"+ reiseklasse.sterne() + "Flug\n\t\t["+preis+ " + " + reiseklasse.aufpreis().multiply(aufpreisFaktor) + "]\n\n";
    }

}
