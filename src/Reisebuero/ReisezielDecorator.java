package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public interface ReisezielDecorator {

    Money getPreis();
    String getBeschreibung();

    void setReiseklasse(Reiseklasse reiseklasse);
    Reiseklasse getReiseklasse();
}
