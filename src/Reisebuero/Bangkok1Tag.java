package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public class Bangkok1Tag extends Orte{

    private final Money preis = new Money(5000);
    private final double aufpreisFaktor = 0.3;

    @Override
    public Reiseklasse getReiseklasse() {
        return reisezielDecorator.getReiseklasse();
    }

    @Override
    public void setReiseklasse(Reiseklasse reiseklasse) {
        reisezielDecorator.setReiseklasse(reiseklasse);
    }

    public Bangkok1Tag(ReisezielDecorator reisezielDecorator) {
        super(reisezielDecorator);
    }

    @Override
    public Money getPreis() {
        return reisezielDecorator.getPreis().add(preis.add(reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor)));
    }

    @Override
    public String getBeschreibung() {
        return reisezielDecorator.getBeschreibung() + reisezielDecorator.getReiseklasse().sterne() + "1 Tag\tBangkok:\tDer Startpunkt fast jeder Asienreise.\n\t\t["+preis+ " + " + reisezielDecorator.getReiseklasse().aufpreis().multiply(aufpreisFaktor) + "]\n\n";
    }
}
