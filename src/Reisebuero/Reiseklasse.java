package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public interface Reiseklasse {
    public Money aufpreis();
    public String sterne();
}

class StandardZweiSterne implements Reiseklasse {

    @Override
    public Money aufpreis() {
        return new Money(0);
    }

    @Override
    public String sterne() {
        return "**\t\t";
    }
}

class DreiSterne implements Reiseklasse {

    @Override
    public Money aufpreis() {
        //10€
        return new Money(1000);
    }

    @Override
    public String sterne() {
        return "***\t\t";
    }
}

class VierSterne implements Reiseklasse {

    @Override
    public Money aufpreis() {
        //15€
        return new Money(1500);
    }

    @Override
    public String sterne() {
        return "****\t";
    }
}

class FuenfSterne implements Reiseklasse {

    @Override
    public Money aufpreis() {
        //30€
        return new Money(3000);
    }

    @Override
    public String sterne() {
        return "*****\t";
    }
}
