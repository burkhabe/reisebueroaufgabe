package Reisebuero;

/**
 * Created by benni on 07.05.17.
 */
public interface AbstractPauschalreiseFactory {
    ReisezielDecorator generiere7Tage(Reiseklasse reiseklasse);
    ReisezielDecorator generiere10Tage(Reiseklasse reiseklasse);
    ReisezielDecorator generiere16Tage(Reiseklasse reiseklasse);
    void druckeBeschreibung(ReisezielDecorator reisezielDecorator);
}
